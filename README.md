# sqlite-pro-wrapper

A simple SQLite wrapper for Node.js with support for TypeScript.

## Installation

To install this package, run the following command:

```
npm install sqlite-pro-wrapper
```

## Usage

Here's an example of how to use the `sqlite-pro-wrapper` module to connect to a database, create a table, insert some data, and query the data:

```javascript
import sqliteProWrapper from 'sqlite-pro-wrapper';

// Connect to the database
const db = sqliteProWrapper.connect('my_database.db');

// Create a table
await db.run(`CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  age INTEGER NOT NULL
)`);

// Insert some data
await db.run(`INSERT INTO users (name, age) VALUES ('John', 30)`);
await db.run(`INSERT INTO users (name, age) VALUES ('Jane', 25)`);

// Query the data
const users =
  (await db.all) <
  { id: number, name: string, age: number } >
  `SELECT * FROM users`;
console.log(users);
// Output: [ { id: 1, name: 'John', age: 30 }, { id: 2, name: 'Jane', age: 25 } ]

// Close the database connection
db.close();
```

## API

### Database

The `Database` interface represents a connection to an SQLite database. It has the following methods:

#### all<T>(sql: string, params?: any[]): Promise<T[]>

Executes a SQL query and returns an array of results. The query can contain placeholders (`?`) for values that should be escaped. The `params` array contains the values to substitute for the placeholders.

#### run(sql: string, params?: any[]): Promise<any>

Executes a SQL query that does not return a result set, such as an INSERT, UPDATE, or DELETE statement. Returns a promise that resolves to the result of the query.

#### get<T>(sql: string, params?: any[]): Promise<T>

Executes a SQL query that returns a single row. The query can contain placeholders (`?`) for values that should be escaped. The `params` array contains the values to substitute for the placeholders.

#### close(): Promise<void>

Closes the database connection.

### SqliteWrapper

The `SqliteWrapper` interface provides methods for working with SQLite databases. It has the following methods:

#### connect(databasePath: string, sqlDir?: string): Database

Connects to an SQLite database at the specified path. If `sqlDir` is provided, the wrapper will automatically execute any SQL files in that directory when the connection is established.

#### create<T>(table: string, data: Omit<T, "id">, db: Database): Promise<T>

Creates a new row in the specified table with the provided data. Returns a promise that resolves to the new row.

#### update<T>(table: string, data: Partial<T>, searchColumn: string, searchValue: any, db: Database): Promise<T | undefined>

Updates a row in the specified table with the provided data. The `searchColumn` and `searchValue` parameters are used to identify the row to update. Returns a promise that resolves to the updated row, or `undefined` if no matching row was found.

#### get<T>(table: string , db: Database ,searchColumn: string, searchValue: any): Promise<T |T[] | undefined>

Gets a single row or multiple rows from the specified table based on the provided search criteria. The `searchColumn` and `searchValue` parameters are used to identify the row(s) to retrieve. Returns a promise that resolves to the matching row(s), or `undefined` if no matching row was found.

#### delete<T>(table: string ,searchColumn: string, searchValue: any, db: Database): Promise<string>

Deletes a row from the specified table based on the provided search criteria. The `searchColumn` and `searchValue` parameters are used to identify the row to delete. Returns a promise that resolves to a string indicating the number of rows affected.

#### getAll<T>(table: string,db:Database):Promise<T | T[] | undefined>

Gets all rows from the specified table. Returns a promise that resolves to an array of matching rows, or `undefined` if no rows were found.

## TypeScript Support

This package provides full TypeScript support, including interfaces for all methods and types for all callbacks and promises.

## License

This package is licensed under the MIT License. See the `LICENSE` file for more information.

# sqlite-pro-wrapper

Un wrapper simple pour SQLite en Node.js avec support pour TypeScript.

## Installation

Pour installer ce package, exécutez la commande suivante :

```
npm install sqlite-pro-wrapper
```

## Utilisation

Voici un exemple d'utilisation du module `sqlite-pro-wrapper` pour se connecter à une base de données, créer une table, insérer des données et effectuer des requêtes :

```javascript
import sqliteProWrapper from 'sqlite-pro-wrapper';

// Connexion à la base de données
const db = sqliteProWrapper.connect('ma_base_donnees.db');

// Création d'une table
await db.run(`CREATE TABLE utilisateurs (
  id INTEGER PRIMARY KEY,
  nom TEXT NOT NULL,
  age INTEGER NOT NULL
)`);

// Insertion de données
await db.run(`INSERT INTO utilisateurs (nom, age) VALUES ('Jean', 30)`);
await db.run(`INSERT INTO utilisateurs (nom, age) VALUES ('Marie', 25)`);

// Requête de données
const utilisateurs =
  (await db.all) <
  { id: number, nom: string, age: number } >
  `SELECT * FROM utilisateurs`;
console.log(utilisateurs);
// Sortie : [ { id: 1, nom: 'Jean', age: 30 }, { id: 2, nom: 'Marie', age: 25 } ]

// Fermeture de la connexion à la base de données
db.close();
```

## API

### Database

L'interface `Database` représente une connexion à une base de données SQLite. Elle contient les méthodes suivantes :

#### all<T>(sql: string, params?: any[]): Promise<T[]>

Exécute une requête SQL et renvoie un tableau de résultats. La requête peut contenir des marqueurs de position (`?`) pour les valeurs qui doivent être échappées. Le tableau `params` contient les valeurs à substituer aux marqueurs de position.

#### run(sql: string, params?: any[]): Promise<any>

Exécute une requête SQL qui ne renvoie pas de jeu de résultats, comme une instruction INSERT, UPDATE ou DELETE. Renvoie une promesse qui se résout en le résultat de la requête.

#### get<T>(sql: string, params?: any[]): Promise<T>

Exécute une requête SQL qui renvoie une seule ligne. La requête peut contenir des marqueurs de position (`?`) pour les valeurs qui doivent être échappées. Le tableau `params` contient les valeurs à substituer aux marqueurs de position.

#### close(): Promise<void>

Ferme la connexion à la base de données.

### SqliteWrapper

L'interface `SqliteWrapper` fournit des méthodes pour travailler avec les bases de données SQLite. Elle contient les méthodes suivantes :

#### connect(databasePath: string, sqlDir?: string): Database

Se connecte à une base de données SQLite au chemin spécifié. Si `sqlDir` est fourni, le wrapper exécutera automatiquement tous les fichiers SQL dans ce répertoire lorsque la connexion sera établie.

#### create<T>(table: string, data: Omit<T, "id">, db: Database): Promise<T>

Crée une nouvelle ligne dans la table spécifiée avec les données fournies. Renvoie une promesse qui se résout en la nouvelle ligne.

#### update<T>(table: string, data: Partial<T>, searchColumn: string, searchValue: any, db: Database): Promise<T | undefined>

Met à jour une ligne dans la table spécifiée avec les données fournies. Les paramètres `searchColumn` et `searchValue` sont utilisés pour identifier la ligne à mettre à jour. Renvoie une promesse qui se résout en la ligne mise à jour, ou `undefined` si aucune ligne correspondante n'a été trouvée.

#### get<T>(table: string , db: Database ,searchColumn: string, searchValue: any): Promise<T |T[] | undefined>

Récupère une seule ligne ou plusieurs lignes de la table spécifiée en fonction des critères de recherche fournis. Les paramètres `searchColumn` et `searchValue` sont utilisés pour identifier la ou les lignes à récupérer. Renvoie une promesse qui se résout en la ou les lignes correspondantes, ou `undefined` si aucune ligne correspondante n'a été trouvée.

#### delete<T>(table: string ,searchColumn: string, searchValue: any, db: Database): Promise<string>

Supprime une ligne de la table spécifiée en fonction des critères de recherche fournis. Les paramètres `searchColumn` et `searchValue` sont utilisés pour identifier la ligne à supprimer. Renvoie une promesse qui se résout en une chaîne indiquant le nombre de lignes affectées.

#### getAll<T>(table: string,db:Database):Promise<T | T[] | undefined>

Récupère toutes les lignes de la table spécifiée. Renvoie une promesse qui se résout en un tableau de lignes correspondantes, ou `undefined` si aucune ligne n'a été trouvée.

## Support TypeScript

Ce package fournit un support complet pour TypeScript, y compris des interfaces pour toutes les méthodes et des types pour tous les callbacks et promesses.

## Licence

Ce package est sous licence MIT. Consultez le fichier `LICENSE` pour plus d'informations.
