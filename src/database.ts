import * as fs from 'node:fs';
import * as path from 'node:path';
import * as sqlite3 from 'sqlite3';

export interface Database {
  db : Database;
  all<T>(sql: string, params?: any[]): Promise<T[]>;
  run(sql: string, params?: any[]): Promise<any>;
  get<T>(sql: string, params?: any[]): Promise<T>;
  close(): Promise<void>;
}

export interface SqliteWrapper {
  connect(databasePath: string, sqlDir?: string): Promise<Database>;
  create<T>(table: string, data: Omit<T, 'id'>, db: Database): Promise<T>;
  update<T>(
    table: string,
    data: Partial<T>,
    searchColumn: string,
    searchValue: any,
    db: Database,
  ): Promise<T | undefined>;
  get<T>(
    table: string,
    db: Database,
    searchColumn: string,
    searchValue: any,
  ): Promise<T | T[] | undefined>;
  delete<T>(table: string, id: number, db: Database): Promise<string>;
  getAll<T>(table: string, db: Database): Promise<T | T[] | undefined>;
}

/**
 *  Wrapper Sqlite Pro
 */
const sqliteProWrapper: SqliteWrapper = {
  /**
   * Connect database
   * @param databasePath string
   * @param sqlDir string
   * @returns Database
   */
  async connect(databasePath: string, sqlDir: string = ''): Promise<Database> {
    let db: sqlite3.Database;
    try {
      fs.accessSync(databasePath, fs.constants.R_OK);
      db = new sqlite3.Database(databasePath);
    } catch (e) {
      db = new sqlite3.Database(databasePath);
      const sqlFilesPath = path.resolve(sqlDir);
      if (sqlDir !== '') {
        try {
          const listFiles = fs.readdirSync(sqlDir);
          listFiles.map(async (file: string) => {
            if (path.extname(file) === '.sql') {
              const sqlFile = fs.readFileSync(
                `${sqlFilesPath}/${file}`,
                'utf8',
              ).toString();

              await new Promise((resolve, reject) => {
                db.exec(sqlFile, (err) => {
                  if (err) {
                    reject(err);
                  }
                  resolve(true);
                });
              });
            }
          });
        } catch (error) {
          throw error;
        }
      }
    }

    return {
      /**
       *
       * @param sql String
       * @param params any[]
       * @returns Promise <T[]>
       */
      all<T>(sql: string, params: any[] = []): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
          db.all(sql, params, (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows as T[]);
            }
          });
        });
      },
      /**
       *
       * @param sql string
       * @param params any[]
       * @returns any
       */
      run<T>(sql: string, params: any[] = []): Promise<any> {
        return new Promise<any>((resolve, reject) => {
          db.run(sql, params, function (err) {
            if (err) {
              reject(err);
            } else {
              resolve(this);
            }
          });
        });
      },
      get<T>(sql: string, params: any[] = []): Promise<T> {
        return new Promise<T>((resolve, reject) => {
          db.get(sql, params, (err, row) => {
            if (err) {
              reject(err);
            } else {
              resolve(row as T);
            }
          });
        });
      },
      close(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
          db.close((err) => {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
          });
        });
      },
    } as Database;
  },
  /**
   * create row in table
   * @param table
   * @param data
   * @param db
   * @returns
   */
  async create<T>(
    table: string,
    data: Omit<T, 'id'>,
    db: Database,
  ): Promise<T> {
    const keys = Object.getOwnPropertyNames(data) as (keyof Omit<T, 'id'>)[];
    const values = keys.map((key) => data[key]);
    const placeholders = keys.map(() => '?').join(',');

    try {
      const res = await db.run(
        `INSERT INTO ${table} (${keys.join(',')}) VALUES (${placeholders})`,
        values,
      );
      const lastID = res.lastID;
      const result = await db.get<T>(`SELECT * FROM ${table} WHERE rowid = ?`, [
        lastID,
      ]);
      return result;
    } catch (err: any) {
      throw err;
    }
  },
  /**
   * update row in table
   * @param table
   * @param data
   * @param searchColumn
   * @param searchValue
   * @param db
   * @returns
   */
  async update<T>(
    table: string,
    data: Partial<T>,
    searchColumn: string,
    searchValue: any,
    db: Database,
  ): Promise<T | undefined> {
    const keys = Object.keys(data) as (keyof T)[];
    const values = keys.map((key) => data[key]);
    const setClause = keys.map((key) => `${String(key)}=?`).join(',');

    try {
      const result = await db.get<T>(
        `SELECT * FROM ${table} WHERE ${searchColumn} = ?`,
        [searchValue],
      );

      if (result === undefined) {
        throw new Error('ERROR: no such column: undefined');
      }
      await db.run(`UPDATE ${table} SET ${setClause} WHERE ${searchColumn}=?`, [
        ...values,
        searchValue,
      ]);
      const finalResult = await db.get<T>(
        `SELECT * FROM ${table} WHERE ${searchColumn} = ?`,
        [searchValue],
      );

      return finalResult as T;
    } catch (err: any) {
      throw err;
    }
  },
  /**
   * get data in table
   * @param table
   * @param db
   * @param searchColumn
   * @param searchValue
   * @returns
   */
  async get<T>(
    table: string,
    db: Database,
    searchColumn?: string,
    searchValue: any = [],
  ): Promise<T | T[] | undefined> {
    try {
      let sql = `SELECT * FROM ${table}`;
      if (searchColumn && searchValue) {
        sql += ` WHERE ${searchColumn} = ?`;
      }

      const result = await db.get(sql, searchValue);

      return result as T;
    } catch (error) {
      throw error;
    }
  },
  /**
   * delete row by id in table
   * @param table
   * @param deleteId
   * @param db
   * @returns
   */
  async delete<T>(table: string, deleteId: any, db: Database): Promise<string> {
    try {
      await db.run(`DELETE FROM ${table} WHERE id = ?`, [deleteId]);
      return 'deleted row';
    } catch (error) {
      throw error;
    }
  },

  async getAll<T>(table: string, db: Database): Promise<T | T[] | undefined> {
    try {
      const result = db.all<T>(`SELECT * FROM ${table}`);
      return result;
    } catch (error) {
      throw error;
    }
  },
};

export default sqliteProWrapper;
