import sqliteWrapper, { Database, SqliteWrapper } from './database';

export { Database, SqliteWrapper };
export default sqliteWrapper;
