import sqliteWrapper, { Database } from '../src';
import * as fs from 'fs';
const multiSqlPath = __dirname + '/multiSql';
const sqlPath = __dirname + '/sql';
const badPath = __dirname + '/sal';
const dbPath = 'dbtest.db';
const sleep = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
interface IUser {
  id: number;
  name: string;
  email: string;
}

interface IProduct {
  id : number;
  name: string;
  price : number;
}

describe('sqliteWrapper db', () => {
  it('should create database with sql folder exist with data', async () => {
    let db: Database = {} as Database;
    try {
      db = await sqliteWrapper.connect(dbPath, sqlPath);
    } catch (e) {
      expect(typeof db).toEqual('object');
      const data = await sqliteWrapper.getAll<IUser[]>('Users', db);
      expect(data).toHaveLength(5);
      await db.close().then(() => fs.unlinkSync(dbPath));
    }
  });
  it('should create database with bad sql', async () => {
    let db: Database | null = null;
    try {
      db = await sqliteWrapper.connect(dbPath, badPath);
      expect(typeof db).toEqual('object');
      await db.close().then(() => fs.unlinkSync(dbPath));
    } catch (e) {
      console.log(e);
    }
  });
});
describe('create data base with multi sql file', () => {
  

  it('should create database with sql path with multi files', async () => {
  
    //let db: Database = {} as Database;
    try {
      const db = await sqliteWrapper.connect(':memory:', multiSqlPath);
    
      const data = await sqliteWrapper.getAll<IUser[]>('Users', db);
      expect(data).toHaveLength(5);
      
      const products = await sqliteWrapper.getAll<IProduct[]>('product', db);
      expect(products).toHaveLength(5);
      
      await db.close();
    } catch (e) {
      console.log(e);
    }
  });
});
