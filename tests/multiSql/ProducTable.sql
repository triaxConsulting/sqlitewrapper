create table IF NOT EXISTS product (
	id INT,
	name VARCHAR(50),
	price VARCHAR(50)
);
insert into product (id, name, price) values (1, 'Wine - Prosecco Valdobiaddene', '$6.48');
insert into product (id, name, price) values (2, 'Wine - Remy Pannier Rose', '$4.22');
insert into product (id, name, price) values (3, 'Shopper Bag - S - 4', '$9.54');
insert into product (id, name, price) values (4, 'Cheese - Cheddar, Medium', '$8.59');
insert into product (id, name, price) values (5, 'Squash - Pepper', '$4.04');