create table IF NOT EXISTS Users (
	id INT NOT NULL PRIMARY KEY,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	email VARCHAR(50)
);
insert into Users (id, first_name, last_name, email) values (1, 'Karlee', 'Assiter', 'kassiter0@hc360.com');
insert into Users(id, first_name, last_name, email) values (2, 'Ethelred', 'Cresswell', 'ecresswell1@wiley.com');
insert into Users (id, first_name, last_name, email) values (3, 'Carla', 'Ridoutt', 'cridoutt2@digg.com');
insert into Users (id, first_name, last_name, email) values (4, 'Florinda', 'Scranney', 'fscranney3@g.co');
insert into Users (id, first_name, last_name, email) values (5, 'Janaya', 'Skuse', 'jskuse4@nbcnews.com');
