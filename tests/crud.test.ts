import sqliteWrapper, { Database } from '../src';

import * as path from 'path';
import * as fs from 'fs';

interface User {
  id: number;
  name: string;
  email: string;
}

describe('sqliteWrapper', () => {
  let db: Database;

  beforeAll(async () => {
    const dbPath = 'test.db';

    db = await sqliteWrapper.connect(dbPath);

    await db.run(
      'CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, email TEXT)',
    );
  });

  afterAll(async () => {
    db.close().then(() => fs.unlinkSync('test.db'));
  });

  it('should create a new user without id', async () => {
    const newUser: Omit<User, 'id'> = {
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    const createdUser = await sqliteWrapper.create<User>('users', newUser, db);

    expect(createdUser).toEqual({
      id: expect.any(Number),
      name: newUser.name,
      email: newUser.email,
    });
  });

  it('should update an existing user', async () => {
    const existingUser: User = {
      id: 1,
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    const updatedUser: Partial<User> = {
      name: 'Jane Doe',
    };

    const updatedResult = await sqliteWrapper.update<User>(
      'users',
      updatedUser,
      'id',
      existingUser.id,
      db,
    );

    expect(updatedResult).toEqual({
      id: existingUser.id,
      name: updatedUser.name,
      email: existingUser.email,
    });
  });

  it('should throw an error when updating a non-existent user', async () => {
    const nonExistentUser: Partial<User> = {
      name: 'John Doe',
    };

    await expect(
      sqliteWrapper.update<User>('users', nonExistentUser, 'id', 999, db),
    ).rejects.toThrow('ERROR: no such column: undefined');
  });

  it('should return all users', async () => {
    await db.run('DELETE FROM users');

    const user1: Omit<User, 'id'> = {
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    const user2: Omit<User, 'id'> = {
      name: 'Jane Doe',
      email: 'jane.doe@example.com',
    };

    await sqliteWrapper.create<User>('users', user1, db);
    await sqliteWrapper.create<User>('users', user2, db);

    const allUsers = await sqliteWrapper.getAll<User[]>('Users', db);

    expect(allUsers).toHaveLength(2);
    expect(allUsers).toContainEqual({
      id: expect.any(Number),
      name: user1.name,
      email: user1.email,
    });
    expect(allUsers).toContainEqual({
      id: expect.any(Number),
      name: user2.name,
      email: user2.email,
    });
  });

  it('should return a single user', async () => {
    const user: Omit<User, 'id'> = {
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    try {
      await sqliteWrapper.create<User>('users', user, db);

      const singleUser = await sqliteWrapper.get<User>('users', db, 'email', [
        user.email,
      ]);
      expect(singleUser).toEqual({
        id: expect.any(Number),
        name: user.name,
        email: user.email,
      });
    } catch (error) {
      throw error;
    }
  });

  it('Create user with id', async () => {
    const newUser: User = {
      id: 15,
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    const createdUser = await sqliteWrapper.create<User>('users', newUser, db);

    expect(createdUser).toEqual({
      id: newUser.id,
      name: newUser.name,
      email: newUser.email,
    });
  });

  it('Delete user by id', async () => {
    const newUser: User = {
      id: 17,
      name: 'John Doe',
      email: 'john.doe@example.com',
    };

    await sqliteWrapper.create<User>('users', newUser, db);
    const result = await sqliteWrapper.delete('users', 15, db);
    expect(result).toEqual('deleted row');
  });
});
